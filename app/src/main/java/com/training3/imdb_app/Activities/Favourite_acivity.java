package com.training3.imdb_app.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.training3.imdb_app.Adapter.Favourite_adapter;
import com.training3.imdb_app.Getter_Setter.Wishlist_model;
import com.training3.imdb_app.R;
import com.training3.imdb_app.database.DatabaseHelper;
import java.util.ArrayList;
import java.util.List;

public class Favourite_acivity extends AppCompatActivity {

    public static List<Wishlist_model> datamodel = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite_acivity);

        setTitle("Favourite Movies");  //Change App Bar Title

        RecyclerView recyclerView = findViewById(R.id.recyclerview_show);
        TextView txt_messgae = findViewById(R.id.txt_addfav);

        DatabaseHelper database = new DatabaseHelper(Favourite_acivity.this);
        datamodel=  database.getdata();  //Get data from Sqlite Database

        if (datamodel.size()==0)   // Check size of list
        {
            txt_messgae.setVisibility(View.VISIBLE);
        }
        else
        {
            txt_messgae.setVisibility(View.GONE);
        }
        Favourite_adapter favourite_adapter = new Favourite_adapter(this, datamodel);
        RecyclerView.LayoutManager reLayoutManager =new GridLayoutManager(getApplicationContext(),2);
        recyclerView.setLayoutManager(reLayoutManager);
        recyclerView.setAdapter(favourite_adapter);
    }


    @Override
    public void onRestart()    // Refresh app after backpressed from another activity
    {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }


}
