package com.training3.imdb_app.Activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.training3.imdb_app.Adapter.Recyclerview_Adapter;
import com.training3.imdb_app.Constant_.Api_interface;
import com.training3.imdb_app.Constant_.Constant;
import com.training3.imdb_app.Getter_Setter.ImdbModel;
import com.training3.imdb_app.Getter_Setter.Result;
import com.training3.imdb_app.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<Result> listinflate = new ArrayList<>();
    private TextView txt_refresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("PopularMovies");  // Change Title of Action bar

        FINDVIEWBYID();

        if (isNetworkConnected()) {    // Checking Internet Availability

            txt_refresh.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            getdata();                  // Fetching data
        } else {

            txt_refresh.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }

        txt_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onResume();

                if (isNetworkConnected())
                {

                    txt_refresh.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
                else
                {

                    txt_refresh.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }
        });
    }

    private void FINDVIEWBYID() {
        recyclerView = findViewById(R.id.recyclerview);
        txt_refresh=findViewById(R.id.txt_refresh);

    }


    private boolean isNetworkConnected() {  //Network Connectivity Method
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        assert cm != null;
        return cm.getActiveNetworkInfo() != null;
    }


    private void getdata() {

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(Constant.CONSTANT).build();
        final Api_interface api_interface = restAdapter.create(Api_interface.class);

        api_interface.getdata("", new Callback<ImdbModel>() {
            @Override
            public void success(ImdbModel imdbModel, Response response) {

                listinflate = imdbModel.getResults();
                recyclerview_(listinflate);
            }
            @Override
            public void failure(RetrofitError error) {

                Toast.makeText(MainActivity.this, "Connect your Internet", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.menus, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.favourite: {
                Intent intent = new Intent(MainActivity.this, Favourite_acivity.class);
                startActivity(intent);
                break;
            }
            case R.id.Populatrity: {

                setTitle("Popular Movies");
                getdata();
                break;
            }
            case R.id.Title: {

                setTitle("By Title");
                Collections.sort(listinflate, new Comparator<Result>() {  // Sorting arraylist by Title in accending order
                    @Override
                    public int compare(Result o1, Result o2) {

                        recyclerview_(listinflate);  // Show data into Recyclerview
                        return o1.getTitle().compareTo(o2.getTitle());
                    }
                });
                break;
            }
            case R.id.Year: {

                setTitle("By Year");
                Collections.sort(listinflate, new Comparator<Result>() {
                    @Override
                    public int compare(Result o1, Result o2) {  // Sorting arraylist by Year in Accending order

                        recyclerview_(listinflate);
                        return o1.getReleaseDate().substring(0, 4).compareTo(o2.getReleaseDate().substring(0, 4));
                    }
                });
                break;
            }
            case R.id.topdated: {

                setTitle("Top Rated");
                Collections.sort(listinflate, new Comparator<Result>() {  // Sorting arraylist by Rating
                    @Override
                    public int compare(Result o1, Result o2) {


                        ArrayList<Result> tempElements = new ArrayList<>(listinflate);
                        Collections.reverse(tempElements);  // Reverse the Sorted arraylist
                        recyclerview_(tempElements);
                        return o1.getVoteAverage().compareTo(o2.getVoteAverage());
                    }
                });
                break;
            }
        }
        return super.onOptionsItemSelected(item);

    }

    public void recyclerview_(List<Result> list) {   // Method for Recyclerview

        recyclerView.setVisibility(View.VISIBLE);
        Recyclerview_Adapter recyclerview_adapter = new Recyclerview_Adapter(MainActivity.this, list);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(MainActivity.this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(recyclerview_adapter);
        recyclerview_adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();

        setTitle("Popular Movies");
        getdata();
    }
}

