package com.training3.imdb_app.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.training3.imdb_app.Getter_Setter.Wishlist_model;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {


    public static String DATABASE = "imdb.db";
    public static String ID = "id";
    public static String TABLE = "imdbtable";
    public static String TITLE = "title";
    public static String IMAGE = "image";
    public static String YEAR = "year";
    public static String RATING = "rating";
    public static String OVERVIEW = "overview";
    public static String COUNT = "count";
    public static String AVERAGE = "average";
    String br;

    public DatabaseHelper(Context context) {
        super(context, DATABASE, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {       //  create a table


        String query = "CREATE TABLE " + TABLE + "("
                + ID + " VARCHAR unique, "
                + TITLE + " VARCHAR, "
                + IMAGE + " VARCHAR, "
                + YEAR + " VARCHAR, "
                + RATING + " VARCHAR, "
                + OVERVIEW + " VARCHAR, "
                + COUNT + " VARCHAR, "
                + AVERAGE + " VARCHAR "
                + ");";

        db.execSQL(query);      // execute the query

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE + " ;");
        onCreate(db);
    }

    public void insertdata(String id,String image, String title, String year, String rating, String overview, String count, String average) {

        // Inserting data into sqlite database

        SQLiteDatabase db = this.getWritableDatabase();     // Store data into databse
        ContentValues contentValues = new ContentValues();

        contentValues.put(ID, id);
        contentValues.put(IMAGE, image);
        contentValues.put(TITLE, title);
        contentValues.put(YEAR, year);
        contentValues.put(RATING, rating);
        contentValues.put(OVERVIEW, overview);
        contentValues.put(COUNT, count);
        contentValues.put(AVERAGE, average);
        db.insert(TABLE, null, contentValues);

    }

    public void delete(String id)
    {
        // Inserting data into sqlite database

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE, ID + "=" + id, null);


    }
    public List<Wishlist_model> getdata() {

        // Fetching data from sqlite database

        List<Wishlist_model> data = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase(); // fetching

        String selectQuery = "SELECT  * FROM " + TABLE;
        Cursor cursor = db.rawQuery(selectQuery, null);

        cursor.moveToFirst();       // Cursor move to first position
        StringBuffer stringBuffer = new StringBuffer();
        Wishlist_model dataModel = null;

        while (!cursor.isAfterLast()) {
            dataModel = new Wishlist_model();

            // Traversing the database table
            String id = cursor.getString(cursor.getColumnIndex(ID));
            String image = cursor.getString(cursor.getColumnIndex(IMAGE));
            String title = cursor.getString(cursor.getColumnIndex(TITLE));
            String year = cursor.getString(cursor.getColumnIndex(YEAR));
            String rating = cursor.getString(cursor.getColumnIndex(RATING));
            String overview = cursor.getString(cursor.getColumnIndex(OVERVIEW));
            String count = cursor.getString(cursor.getColumnIndex(COUNT));
            String average = cursor.getString(cursor.getColumnIndex(AVERAGE));
            cursor.moveToNext();

            // Data set with GETTER-SETTER

            dataModel.setId(id);
            dataModel.setImage(image);
            dataModel.setTitle(title);
            dataModel.setYear(year);
            dataModel.setRating(rating);
            dataModel.setOverview(overview);
            dataModel.setCount(count);
            dataModel.setAverage(average);

            stringBuffer.append(dataModel);
            data.add(dataModel);
        }
        cursor.close();
        db.close();

        return data;
    }


}
